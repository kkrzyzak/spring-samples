package com.fp;

import org.springframework.stereotype.Component;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 * @author karol.krzyzak@soft-k.pl
 */

@ManagedBean(name = "developerBean")
@RequestScoped
@Component
public class JSFDeveloperBean {

	public JSFDeveloperBean() {
		this.name = "Karol";
	}

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}

package com.fp.config;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.faces.webapp.FacesServlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

/**
 * @author karol.krzyzak@soft-k.pl
 */
public class Initializer implements WebApplicationInitializer {

	@Override
	public void onStartup(javax.servlet.ServletContext servletContext) throws ServletException {

		AnnotationConfigWebApplicationContext context =
				new AnnotationConfigWebApplicationContext();

		context.register(AppConfiguration.class);
		context.setServletContext(servletContext);

		servletContext.addListener(new ContextLoaderListener(context));
		servletContext.addListener(new RequestContextListener());

		ServletRegistration.Dynamic dispatcher =
				servletContext.addServlet("DispatcherServlet", new DispatcherServlet(context));
		dispatcher.setLoadOnStartup(1);
		dispatcher.addMapping("/");

		/** Faces Servlet */
		ServletRegistration.Dynamic facesServlet = servletContext.addServlet("Faces Servlet", FacesServlet.class);
		facesServlet.setLoadOnStartup(1);
		facesServlet.addMapping("*.xhtml");

	}

}

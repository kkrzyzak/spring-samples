package com.fp.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author karol.krzyzak@soft-k.pl
 */
@RestController
public class SimpleRestController {

	@RequestMapping("/rest")
	public String getName() {
		return "Karol";

	}

}



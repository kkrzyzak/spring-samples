Compile
------------------
mvn clean install

Run
------------------
mvn tomee:run

Test
------------------

REST
http://localhost:8080/spring42/rest

JSP
http://localhost:8080/spring42/jsp/
or
http://localhost:8080/spring42/jsp/Karol

JSF
http://localhost:8080/spring42/user.xhtml

